<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\clients;

use App\products;

use App\clients_has_products;

use Excel;

use Mail;

class DataController extends Controller
{
    
    public function load(Request $request){
 		//downloading the files
		$route = "../storage/xls/";
		$files = array("clients.xlsx","products.xlsx");
		/*foreach($files as $file){
			Curl::to('https://dl.dropboxusercontent.com/u/5274213/'.$file)
			    ->enableDebug($route.'/log/log'.$file.'.txt')
		        ->download($route.$file);
		    }*/
		//reading the files
	    Excel::selectSheets('Sheet1')->load($route.$files[1], function($reader) {
		  			$clients = $reader->get()->groupBy('client');
		  			foreach ($clients as $key => $groupBy) {
		  				$client = new clients;
				        $client->name = $key;
				        $client->save();
						$clients[$key]->id=$client->id;
		  			}
		  			$products = $reader->get()->groupBy('product');
		  			foreach ($products as $key => $groupBy) {
		  				$product = new products;
				        $product->name = $key;
				        $product->save();
						$products[$key]->id=$product->id;
		  			}
					// Loop through all rows
	    			$reader->each(function($row) use ($clients,$products) {
	    					$clients_has_products = new clients_has_products;
	    					$clients_has_products->client_id = $clients[$row->client]->id;
	    					$clients_has_products->product_id = $products[$row->product]->id;
	    					$clients_has_products->total = $row->total;
	    					$clients_has_products->date = $row->date;
	    					$clients_has_products->save();
		  			});//end each
		});//end Excel
		return 'Data Loaded successful <br/><a href="'.url("/").'"><< Back Home</a>';
    }
    function mail(Request $request){
    	$user= "user amils";
    	$data = json_decode($request->input('data'));
    	$email ='alexander@webscribble.com';
    	$datas = ['orders'=>$data->orders,'highcharts'=>$data->highcharts,"email"=>$email];
    	Mail::send('mails.reports', $datas, function ($m) use ($email) {
            $m->from('jeanmendozar@TestLaravel.com', 'TestLaravel');
            $m->to($email,'jeanmendoza rivas')->subject('Report Test');
        });
        return view('mails.reports',$datas);
    }
}
