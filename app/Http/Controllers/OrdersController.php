<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\ordersRepository;

use App\clients_has_products;

use App\products;

use Response;

class OrdersController extends Controller
{
    protected $ordersRepository;
    public function __construct(ordersRepository $ordersRepository)
    {
        $this->ordersRepository = $ordersRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->input('keyword');
        $findby  = $request->input('findby');
        $results = $this->ordersRepository->findByKeyword($findby,$keyword);
        $orders  = $results[0];
        $highcharts  = $results[1];
        if($request->ajax()){
            return Response::json(array("orders"=>$orders,"highcharts"=>$highcharts), 200);
        }
        $data    = ['orders'=>$orders,'highcharts'=>$highcharts];
        return view('orders',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order  = clients_has_products::find($id);   
        $order->total = $request->input('total');
        $array   = explode("/",$request->input('date'));
        $date    = implode("-",array_reverse ($array));
        $order->date = $date;
        //$order->product->name = $request->input('product');
        $order->save();
        //save product
        $product = products::find($order->product_id);
        $product->name = $request->input('product');
        $product->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
            $order  = clients_has_products::find($id);
            $order->delete();
            $message="Your Row was removed successfully";
            if($request->ajax()){
              return response()->json([
                'id'=>$id,
                'message'=>$message
                ]);
            }else{
                abort(403);
              }
    }
}
