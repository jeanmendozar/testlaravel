<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('data/load',array('uses' => 'DataController@load', 'as' => 'data.load'));
Route::post('mail',array('uses' => 'DataController@mail', 'as' => 'data.mail'));
Route::resource('order', 'OrdersController');
Route::resource('/', 'OrdersController');