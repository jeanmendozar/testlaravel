<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class clients_has_products extends Model
{
    public function client()
    {
        return $this->belongsTo('App\clients','client_id');
    }
    public function product()
    {
        return $this->belongsTo('App\products','product_id');
    }
    public function getTotalAttribute($value)
    {
        return '$ '.$value;
    }
    public function getDateAttribute($value)
    {
        return date("d/m/Y",strtotime($value));
    }
}
