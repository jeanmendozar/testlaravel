<?php 

namespace App\Repositories;

use App\clients_has_products;

use DB;

class ordersRepository{

  /**
    returns the collection of orders, according to a custom filter by keywords
    params string findby
    params string keyword
  */
	public  function  findByKeyword($findby,$keyword)  {
        $query = clients_has_products::with('client')
                                     ->with('product');
        $like=null;
        switch ($findby) {
            case 'client':        
                $query->whereHas('client', function ($query) use($keyword){
                    $query->where('name', 'like', '%'.$keyword.'%');
                });
            break;
            case 'product':
                $query->whereHas('product', function ($query) use($keyword){
                    $query->where('name', 'like', '%'.$keyword.'%');
                });
            break;
            case 'total':
                $query->where('total','=',$keyword);
            break;
            case 'date':
                $array   = explode("/",$keyword);
                $keyword = implode("-",array_reverse ($array));
                $like = (count($array)==1)? "%".$keyword."%":"%".$keyword;
                $query->where('date','like', "$like");
            break;
        }
        $orders = $query->get();
        $query->select(DB::raw('sum(total) as sum_total,date'));
        $query->groupby('date');
        if(!is_null($like))
           $query->having('date','like',$like);
        $highcharts = $query->groupby('date')->get();
        foreach ($highcharts as $key => $value) {
            $highcharts[$key]->date=date("m-d-Y",strtotime($value->date));
        }
        //dd($highcharts);
       return array($orders,$highcharts);
    }
  /** saving everything related to the order */
  function saveAllAttributeByOrder(){
      
  }
}