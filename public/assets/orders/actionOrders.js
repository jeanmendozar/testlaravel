function get_data_form(obj,id_form){
    var id = obj.data("id");
    var form = $("#form-"+id_form);
    var url  = form.attr('action').replace(':ORDER_ID',id);
    var data = form.serialize();
    ret = {
        'url' : url,
        'data' : data,
    };
  return ret;
}
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
function make_data_higchart(datas){
    categories = [];
    data = [];
    $.each(datas,function(i,obj){
      categories.push(obj.date);
      data.push(parseInt(obj.sum_total));
    });
    return {"categories":categories,"data":data};
}
$(document).ready(function() {
        var chart;
        var table = $('#example').DataTable( {
          lengthChange:false,
          searching:false
          } );
          fila = '#example tbody tr';
          $(document).on( 'click', '.del', function (e) {
                e.preventDefault();
                $(fila).removeClass('selected');
                table.$('tr.selected').removeClass('selected');
                mi_fil = $(this).parents('tr');
                mi_fil.addClass('selected');
                if(confirm("You want to delete this row?")){
                  ret = get_data_form($(this),'delete_orders');
                  $.post(ret["url"],ret["data"],function(result){
                     table.row('.selected').remove().draw( false );
                     alert(result.message);
                     data = {
                                'keyword':getUrlParameter('keyword'),
                                'findby':getUrlParameter('findby'),
                              }
                     $.get('/',data,function(result){
                          datas = make_data_higchart(result.highcharts);
                          chart.series[0].setData(datas.data);
                          chart.xAxis[0].setCategories(datas.categories);
                          //chart.redraw();
                      });
                  }).fail(function(){
                      alert("The row has not been deleted");
                  });
                }
                else{
                  $(fila).removeClass('selected');
                }
          });
  /*function highcharts*/
    content_highcharts = JSON.parse($("#content_highcharts").html());
    if(content_highcharts.length > 0 ){
        datas = make_data_higchart(content_highcharts);
        $('#container').removeClass('hide');
        options_highcharts = {
            chart: {
                renderTo: 'container',
                type: 'line'
            },
            title: {
                text: 'Sum Total by Date'
            },
            xAxis: {
                categories: datas.categories
            },
            yAxis: {
                title: {
                    text: 'Total'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                name: 'Date',
                data: datas.data
            }]
        };
        chart = new Highcharts.Chart(options_highcharts);
      }
    /**edit modal*/
    var tr = null;
    var modal = null;
    $('#editModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var id = button.data('id') // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      tr = button.parents("tr");
      client = tr.children('td:eq(0)').text();
      product = tr.children('td:eq(1)').text();
      total = tr.children('td:eq(2)').text();
      date = tr.children('td:eq(3)').text();
      modal = $(this)
      modal.find('.modal-body #id_order').val(id)
      modal.find('.modal-body #client').val(client)
      modal.find('.modal-body #product').val(product)
      modal.find('.modal-body #total').val(total.substring(2))
      modal.find('.modal-body #date').val(date)
    });
    $("#saveModal").on('click',function(e){
      e.preventDefault();
           var id   = $("#id_order").val();
           var form = $("#form-update_order");
           var url  = form.attr('action').replace(':ORDER_ID',id);
           var data = form.serialize();
                  $.post(url,data,function(result){
                      $('#editModal').modal('hide');
                      tr.children('td:eq(1)').text($("#product").val());
                      tr.children('td:eq(2)').text("$ "+$("#total").val());
                      tr.children('td:eq(3)').text($("#date").val());
                      /*update higcharts*/
                      data = {
                                'keyword':getUrlParameter('keyword'),
                                'findby':getUrlParameter('findby'),
                              }
                      $.get('/',data,function(result){
                          datas = make_data_higchart(result.highcharts);
                          chart.series[0].setData(datas.data);
                          chart.xAxis[0].setCategories(datas.categories);
                          //chart.redraw();
                      });
                  }).fail(function(){
                      alert("The row has not been updated");
                  });
    })
    $("#email_report").on("click",function(e){
      e.preventDefault();
      $.get('/',data,function(result){
          dataJson=JSON.stringify(result);
          $("#data").val(dataJson);
          $("#form-mail_report").submit();
      });
    });
});