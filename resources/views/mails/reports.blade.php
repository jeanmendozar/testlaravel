<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Test Laravel by jeanmendozar@gmail.com">
    <meta name="author" content="jeanmendozar@gmail.com; skype : jean1187">
    <link rel="icon" href="../../favicon.ico">
    <title>Test Laravel</title>
    

  <link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.material.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://code.jquery.com/jquery-2.2.3.min.js" ></script>
  
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">
  </script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.11/js/dataTables.material.min.js"></script>
    <script type="text/javascript">
      $(function(){
        var table = $('#example').DataTable( {
          //lengthChange:false,
          //searching:false
          } );
      })
    </script>
  </head>
  <body>
    <!-- Begin page content -->
    <div class="container">
      <table id="example" class="mdl-data-table" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Client</th>
            <th>Product</th>
            <th>Total</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($orders as $order)
              <tr>
                <td>{{ $order->client->name }}</td>
                <td>{{ $order->product->name }}</td>
                <td>{{ $order->total }}</td>
                <td>{{ $order->date }}</td>
              </tr>
          @endforeach
        </tbody>
      </table>
      </table>
      Your email has been successfully sent to {{ $email }}<br/><a href="'.url("/").'"><< Back Home</a>
    </div>
    <!--<footer class="footer">
      <div class="container text-center">
        <p class="text-muted">by jeanmendozar@gmail.com  |  skype: jean1187</p>
      </div>
    </footer>-->
  </body>
</html>
