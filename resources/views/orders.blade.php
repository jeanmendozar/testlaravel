@extends('base')
@section('container')
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/table.css')}} ">
    <!--<link rel="stylesheet" type="text/css" href="{{ asset('/assets/material.min.css')}} "-->
    <link href="{{ asset('/assets/selected.datatables.css')}}" rel="stylesheet">
    <link href="{{ asset('/assets/bower/datatables/media/css/dataTables.material.min.css') }}" rel="stylesheet">
    <script src="{{ asset('/assets/bower/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/assets/bower/datatables/media/js/dataTables.material.min.js') }}"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="{{ asset('/assets/orders/actionOrders.js?v1') }}"></script>
    <div id="content_highcharts" class="hide">{{ $highcharts }}</div>
    <div id="container" class="hide" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
      <a href="#!" id="email_report">Email this report</a>
      <table id="example" class="mdl-data-table rwd-table" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Client</th>
            <th>Product</th>
            <th>Total</th>
            <th>Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($orders as $order)
              <tr>
                <td>{{ $order->client->name }}</td>
                <td>{{ $order->product->name }}</td>
                <td>{{ $order->total }}</td>
                <td>{{ $order->date }}</td>
                <td><a href="#!" data-id="{{ $order->id }}" class="del btn btn-danger btn-small">Del</a>&nbsp;<a href="#!" data-id="{{ $order->id }}" class="btn btn-info btn-small" data-toggle="modal" data-target="#editModal">Edit</a></td>
              </tr>
          @endforeach
        </tbody>
      </table>
      <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="exampleModalLabel">New message</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(['route' => ['order.update', ':ORDER_ID'],'id' => 'form-update_order', 'method' => 'PUT']) !!}
                <input type="hidden"  id="id_order">
                <div class="form-group">
                  <label for="recipient-name" class="control-label">Client:</label>
                  <input type="text" class="form-control" id="client" name="client" disable>
                </div>
                <div class="form-group">
                  <label for="message-text" class="control-label">Product:</label>
                  <input type="text" class="form-control" id="product" name="product" disable>
                </div>
                <div class="form-group">
                  <label for="message-text" class="control-label">Total:</label>
                  <input type="text" class="form-control" id="total" name="total">
                </div>
                <div class="form-group">
                  <label for="message-text" class="control-label">Date:</label>
                  <input type="text" class="form-control" id="date" name="date">
                </div>
              {!! Form::close() !!}
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" id="saveModal" class="btn btn-primary">Save</button>
            </div>
          </div>
        </div>
      </div>
      {!! Form::open(['route' => ['order.destroy', ':ORDER_ID'],'id' => 'form-delete_orders', 'method' => 'DELETE']) !!}
      {!! Form::close() !!}
      {!! Form::open(['route' => ['data.mail'],'id' => 'form-mail_report', 'method' => 'POST']) !!}
        <input type="hidden" name="data" id="data"></input>
      {!! Form::close() !!}
@endsection