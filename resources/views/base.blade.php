<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Test Laravel by jeanmendozar@gmail.com">
    <meta name="author" content="jeanmendozar@gmail.com; skype : jean1187">
    <link rel="icon" href="../../favicon.ico">
    <title>Test Laravel</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/assets/bower/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/bower/bootstrap/dist/css/bootstrap-theme.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/sticky-footer-navbar.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/docs.min.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('/assets/bower/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('/assets/bower/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  </head>
  <body>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Test Laravel</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="{{ url('/') }}">Orders</a></li>
            <li ><a href="{{ url('data/load') }}">Load Data</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <!-- Begin page content -->
    <div class="container">

          <div class="bs-callout bs-callout-info"> 
            <h4 id="callout-progress-csp">Orders</h4> 
              {{ Form::open(array('url' => '.','class'=>'form-inline','method'=>'GET','id'=>"form_search")) }}
                <div class="form-group">
                    <label>Keyword</label>
                    {{  Form::text('keyword','', array('placeholder' => 'Keyword','class'=>'form-control input-lg')) }}
                </div>
                <div class="form-group">
                    <select class="form-control input-lg" name="findby">
                      <option value="all">All</option>
                      <option value="client">Client</option>
                      <option value="product">Product</option>
                      <option value="total">Total</option>
                      <option value="date">Date</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary btn-lg">Search</button>
              {{ Form::close() }}    
          </div>

      @yield('container')
    </div>
    <!--<footer class="footer">
      <div class="container text-center">
        <p class="text-muted">by jeanmendozar@gmail.com  |  skype: jean1187</p>
      </div>
    </footer>-->
  </body>
</html>
